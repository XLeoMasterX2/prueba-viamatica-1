import {Usuario} from "./usuario";

export interface Sesione {
  id?: number;
  fechaIngreso: string;
  fechaCierre: string;
  idUsuario?: Usuario;
}
