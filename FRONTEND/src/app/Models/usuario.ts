import {Persona} from "./persona";

export interface Usuario {
  id: number;
  username: string;
  password: string;
  mail: string;
  status: string;
  intentosFallidos: string;
  maxIntentosFallidos: string;
  idpersona: Persona;
}
