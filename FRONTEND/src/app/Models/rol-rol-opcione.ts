import {RolRolOpcioneId} from "./rol-rol-opcione-id";
import {Rol} from "./rol";

export interface RolRolOpcione {
  id: RolRolOpcioneId;
  idRol: Rol;
}
