export interface Persona {
  id: number;
  nombres: string;
  apellidos: string;
  identificacion: string;
  fechaNacimiento: Date;
}
