import {RolUsuarioId} from "./rol-usuario-id";
import {Rol} from "./rol";

export interface RolUsuario {
  id: RolUsuarioId;
  idRol: Rol;
}
