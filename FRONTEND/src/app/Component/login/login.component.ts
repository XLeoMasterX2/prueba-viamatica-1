import { Component } from '@angular/core';
import {UsuarioService} from "../../Services/Usuario/usuario.service";
import {LoginService} from "../../Services/Login/login.service";
import {FormsModule} from "@angular/forms";
import {MatFormField} from "@angular/material/form-field";
import {MatCard, MatCardContent, MatCardTitle} from "@angular/material/card";
import {MatInput} from "@angular/material/input";
import {MatButton} from "@angular/material/button";
import {Sesione} from "../../Models/sesione";

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    FormsModule,
    MatFormField,
    MatCardContent,
    MatCardTitle,
    MatCard,
    MatInput,
    MatButton
  ],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {
  username: string = '';
  password: string = '';
  sesion: Sesione | null = null;


  constructor(private authService: LoginService) {
  }

  login() {
    if (this.username && this.password) {
      const sesion = this.authService.login(this.username, this.password);

      if (sesion) {
        localStorage.setItem('sesion', JSON.stringify(sesion));
      } else {
        alert('Credenciales incorrectas');
      }
    } else {
      alert('Ingrese usuario y contraseña');
    }
  }

  logout() {
    if (this.username && this.password) {
      // @ts-ignore
      this.sesion = JSON.parse(localStorage.getItem('sesion')) as Sesione;
      this.authService.logout(this.sesion.id);
    }
  }
}
