package com.leonel.viamatica.backend.models;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import org.hibernate.Hibernate;

import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class RolRolOpcioneId implements Serializable {
    private static final long serialVersionUID = -920835637782551883L;
    @Column(name = "idRol", nullable = false)
    private Integer idRol;

    @Column(name = "idOpcion", nullable = false)
    private Integer idOpcion;

    public Integer getIdRol() {
        return idRol;
    }

    public void setIdRol(Integer idRol) {
        this.idRol = idRol;
    }

    public Integer getIdOpcion() {
        return idOpcion;
    }

    public void setIdOpcion(Integer idOpcion) {
        this.idOpcion = idOpcion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        RolRolOpcioneId entity = (RolRolOpcioneId) o;
        return Objects.equals(this.idRol, entity.idRol) &&
                Objects.equals(this.idOpcion, entity.idOpcion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idRol, idOpcion);
    }

}