package com.leonel.viamatica.backend.services;

import com.leonel.viamatica.backend.models.RolUsuario;
import com.leonel.viamatica.backend.models.RolUsuarioId;
import com.leonel.viamatica.backend.repositories.RolUsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class RolUsuarioService {

    @Autowired
    private RolUsuarioRepository rolUsuarioRepository;

    public List<RolUsuario> getAllRolUsuarios() {
        return rolUsuarioRepository.findAll();
    }

    public Optional<RolUsuario> getRolUsuarioById(RolUsuarioId id) {
        return rolUsuarioRepository.findById(id);
    }

    public RolUsuario createRolUsuario(RolUsuario rolUsuario) {
        return rolUsuarioRepository.save(rolUsuario);
    }

    public RolUsuario updateRolUsuario(RolUsuarioId id, RolUsuario nuevoRolUsuario) {
        if (rolUsuarioRepository.existsById(id)) {
            nuevoRolUsuario.setId(id);
            return rolUsuarioRepository.save(nuevoRolUsuario);
        } else {
            throw new IllegalArgumentException("El RolUsuario con ID " + id + " no existe");
        }
    }

    public void deleteRolUsuario(RolUsuarioId id) {
        rolUsuarioRepository.deleteById(id);
    }
}
