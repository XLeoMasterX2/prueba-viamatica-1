package com.leonel.viamatica.backend.util;

import org.springframework.beans.BeanUtils;

public class ConverterUtils
{
    public static <D, E> E convertToEntity(D dto, Class<E> entityClass) {
        try {
            E entity = entityClass.getDeclaredConstructor().newInstance();
            BeanUtils.copyProperties(entity, dto);
            return entity;
        } catch (Exception e) {
            throw new RuntimeException("Error al convertir DTO a entidad", e);
        }
    }

    public static <D, E> D convertToDto(E entity, Class<D> dtoClass) {
        try {
            D dto = dtoClass.getDeclaredConstructor().newInstance();
            BeanUtils.copyProperties(dto, entity);
            return dto;
        } catch (Exception e) {
            throw new RuntimeException("Error al convertir entidad a DTO", e);
        }
    }
}
