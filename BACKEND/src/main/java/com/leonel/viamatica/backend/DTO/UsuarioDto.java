package com.leonel.viamatica.backend.DTO;

import java.io.Serializable;
import java.util.Objects;

public class UsuarioDto implements Serializable {
    private final Integer id;
    private final String username;
    private final String password;
    private final String mail;
    private final Character status;
    private final String intentosFallidos;
    private final String maxIntentosFallidos;
    private final PersonaDto idpersona;

    public UsuarioDto(Integer id, String username, String password, String mail, Character status, String intentosFallidos, String maxIntentosFallidos, PersonaDto idpersona) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.mail = mail;
        this.status = status;
        this.intentosFallidos = intentosFallidos;
        this.maxIntentosFallidos = maxIntentosFallidos;
        this.idpersona = idpersona;
    }

    public Integer getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getMail() {
        return mail;
    }

    public Character getStatus() {
        return status;
    }

    public String getIntentosFallidos() {
        return intentosFallidos;
    }

    public String getMaxIntentosFallidos() {
        return maxIntentosFallidos;
    }

    public PersonaDto getIdpersona() {
        return idpersona;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsuarioDto entity = (UsuarioDto) o;
        return Objects.equals(this.id, entity.id) &&
                Objects.equals(this.username, entity.username) &&
                Objects.equals(this.password, entity.password) &&
                Objects.equals(this.mail, entity.mail) &&
                Objects.equals(this.status, entity.status) &&
                Objects.equals(this.intentosFallidos, entity.intentosFallidos) &&
                Objects.equals(this.maxIntentosFallidos, entity.maxIntentosFallidos) &&
                Objects.equals(this.idpersona, entity.idpersona);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, password, mail, status, intentosFallidos, maxIntentosFallidos, idpersona);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "id = " + id + ", " +
                "username = " + username + ", " +
                "password = " + password + ", " +
                "mail = " + mail + ", " +
                "status = " + status + ", " +
                "intentosFallidos = " + intentosFallidos + ", " +
                "maxIntentosFallidos = " + maxIntentosFallidos + ", " +
                "idpersona = " + idpersona + ")";
    }
}