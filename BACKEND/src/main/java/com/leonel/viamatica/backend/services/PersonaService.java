package com.leonel.viamatica.backend.services;

import com.leonel.viamatica.backend.models.Persona;
import com.leonel.viamatica.backend.repositories.PersonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PersonaService {

    @Autowired
    private PersonaRepository personaRepository;

    public List<Persona> getAllPersonas() {
        return personaRepository.findAll();
    }

    public Optional<Persona> getPersonaById(Integer id) {
        return personaRepository.findById(id);
    }

    public Persona createPersona(Persona persona) {
        return personaRepository.save(persona);
    }

    public Persona updatePersona(Integer id, Persona nuevaPersona) {
        if (personaRepository.existsById(id)) {
            nuevaPersona.setId(id);
            return personaRepository.save(nuevaPersona);
        } else {
            throw new IllegalArgumentException("La persona con ID " + id + " no existe");
        }
    }

    public void deletePersona(Integer id) {
        personaRepository.deleteById(id);
    }
}
