package com.leonel.viamatica.backend.validations;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

public class JsonValidate
{
    private static final ThreadLocal<String> error = new ThreadLocal<>();

    public static String getErrorMessage()
    {
        return error.get();
    }

    public static boolean isValid(JsonNode data,String PathSchema) throws ProcessingException {
        JsonSchema schema = null;
        try {
        schema = JsonSchemaFactory.byDefault().getJsonSchema(PathSchema);
        schema.validate(data);
        return true;
        } catch (Exception e)
        {
        error.set(e.getMessage());
        return false;
        }
    }
}
