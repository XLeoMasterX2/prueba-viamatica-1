package com.leonel.viamatica.backend.repositories;

import com.leonel.viamatica.backend.models.Sesione;
import com.leonel.viamatica.backend.models.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SesioneRepository extends JpaRepository<Sesione, Integer> {
    Sesione findFirstByIdUsuario_UsernameOrderByFechaIngresoDesc(String username);

}