package com.leonel.viamatica.backend.DTO;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

public class PersonaDto implements Serializable {
    private final Integer id;
    private final String nombres;
    private final String apellidos;
    private final String identificacion;
    private final Instant fechaNacimiento;

    public PersonaDto(Integer id, String nombres, String apellidos, String identificacion, Instant fechaNacimiento) {
        this.id = id;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.identificacion = identificacion;
        this.fechaNacimiento = fechaNacimiento;
    }

    public Integer getId() {
        return id;
    }

    public String getNombres() {
        return nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public Instant getFechaNacimiento() {
        return fechaNacimiento;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonaDto entity = (PersonaDto) o;
        return Objects.equals(this.id, entity.id) &&
                Objects.equals(this.nombres, entity.nombres) &&
                Objects.equals(this.apellidos, entity.apellidos) &&
                Objects.equals(this.identificacion, entity.identificacion) &&
                Objects.equals(this.fechaNacimiento, entity.fechaNacimiento);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombres, apellidos, identificacion, fechaNacimiento);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "id = " + id + ", " +
                "nombres = " + nombres + ", " +
                "apellidos = " + apellidos + ", " +
                "identificacion = " + identificacion + ", " +
                "fechaNacimiento = " + fechaNacimiento + ")";
    }
}