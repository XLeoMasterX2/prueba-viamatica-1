package com.leonel.viamatica.backend.controllers;

import com.leonel.viamatica.backend.models.Sesione;
import com.leonel.viamatica.backend.services.SesioneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/sesiones")
public class SesioneController {

    @Autowired
    private SesioneService sesioneService;

    @GetMapping
    public List<Sesione> getAllSesiones() {
        return sesioneService.getAllSesiones();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Sesione> getSesioneById(@PathVariable Integer id) {
        Optional<Sesione> sesione = sesioneService.getSesioneById(id);
        return sesione.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    public ResponseEntity<Sesione> createSesione(@RequestBody Sesione sesione) {
        Sesione createdSesione = sesioneService.createSesione(sesione);
        return new ResponseEntity<>(createdSesione, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Sesione> updateSesione(@PathVariable Integer id, @RequestBody Sesione nuevaSesione) {
        try {
            Sesione updatedSesione = sesioneService.updateSesione(id, nuevaSesione);
            return new ResponseEntity<>(updatedSesione, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteSesione(@PathVariable Integer id) {
        sesioneService.deleteSesione(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
