package com.leonel.viamatica.backend.controllers;

import com.leonel.viamatica.backend.models.ParametrosSistema;
import com.leonel.viamatica.backend.services.ParametrosSistemaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/parametros")
public class ParametrosSistemaController {

    @Autowired
    private ParametrosSistemaService parametrosSistemaService;

    @GetMapping
    public List<ParametrosSistema> getAllParametros() {
        return parametrosSistemaService.getAllParametros();
    }

    @GetMapping("/{id}")
    public ResponseEntity<ParametrosSistema> getParametroById(@PathVariable String id) {
        Optional<ParametrosSistema> parametro = parametrosSistemaService.getParametroById(id);
        return parametro.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    public ResponseEntity<ParametrosSistema> createParametro(@RequestBody ParametrosSistema parametro) {
        ParametrosSistema createdParametro = parametrosSistemaService.createParametro(parametro);
        return new ResponseEntity<>(createdParametro, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ParametrosSistema> updateParametro(@PathVariable String id, @RequestBody ParametrosSistema nuevoParametro) {
        try {
            ParametrosSistema updatedParametro = parametrosSistemaService.updateParametro(id, nuevoParametro);
            return new ResponseEntity<>(updatedParametro, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteParametro(@PathVariable String id) {
        parametrosSistemaService.deleteParametro(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
