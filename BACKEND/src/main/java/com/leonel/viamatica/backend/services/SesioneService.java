package com.leonel.viamatica.backend.services;

import com.leonel.viamatica.backend.models.Sesione;
import com.leonel.viamatica.backend.repositories.SesioneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SesioneService {

    @Autowired
    private SesioneRepository sesioneRepository;

    public List<Sesione> getAllSesiones() {
        return sesioneRepository.findAll();
    }

    public Optional<Sesione> getSesioneById(Integer id) {
        return sesioneRepository.findById(id);
    }

    public Sesione createSesione(Sesione sesione) {
        return sesioneRepository.save(sesione);
    }

    public Sesione updateSesione(Integer id, Sesione nuevaSesione) {
        if (sesioneRepository.existsById(id)) {
            nuevaSesione.setId(id);
            return sesioneRepository.save(nuevaSesione);
        } else {
            throw new IllegalArgumentException("La Sesione con ID " + id + " no existe");
        }
    }

    public void deleteSesione(Integer id) {
        sesioneRepository.deleteById(id);
    }
}
