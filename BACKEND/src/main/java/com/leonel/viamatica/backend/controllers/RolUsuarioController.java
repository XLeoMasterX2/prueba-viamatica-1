package com.leonel.viamatica.backend.controllers;

import com.leonel.viamatica.backend.models.RolUsuario;
import com.leonel.viamatica.backend.models.RolUsuarioId;
import com.leonel.viamatica.backend.services.RolUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/roles-usuarios")
public class RolUsuarioController {

    @Autowired
    private RolUsuarioService rolUsuarioService;

    @GetMapping
    public List<RolUsuario> getAllRolUsuarios() {
        return rolUsuarioService.getAllRolUsuarios();
    }

    @GetMapping("/{rolId}/{usuarioId}")
    public ResponseEntity<RolUsuario> getRolUsuarioById(@PathVariable Integer rolId, @PathVariable Integer usuarioId) {
        RolUsuarioId id = new RolUsuarioId();
        id.setIdRol(rolId);
        id.setIdUsuario(usuarioId);
        Optional<RolUsuario> rolUsuario = rolUsuarioService.getRolUsuarioById(id);
        return rolUsuario.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    public ResponseEntity<RolUsuario> createRolUsuario(@RequestBody RolUsuario rolUsuario) {
        RolUsuario createdRolUsuario = rolUsuarioService.createRolUsuario(rolUsuario);
        return new ResponseEntity<>(createdRolUsuario, HttpStatus.CREATED);
    }

    @PutMapping("/{rolId}/{usuarioId}")
    public ResponseEntity<RolUsuario> updateRolUsuario(@PathVariable Integer rolId, @PathVariable Integer usuarioId, @RequestBody RolUsuario nuevoRolUsuario) {
        RolUsuarioId id = new RolUsuarioId();
        id.setIdRol(rolId);
        id.setIdUsuario(usuarioId);
        try {
            RolUsuario updatedRolUsuario = rolUsuarioService.updateRolUsuario(id, nuevoRolUsuario);
            return new ResponseEntity<>(updatedRolUsuario, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{rolId}/{usuarioId}")
    public ResponseEntity<Void> deleteRolUsuario(@PathVariable Integer rolId, @PathVariable Integer usuarioId) {
        RolUsuarioId id = new RolUsuarioId();
        id.setIdRol(rolId);
        id.setIdUsuario(usuarioId);
        rolUsuarioService.deleteRolUsuario(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
