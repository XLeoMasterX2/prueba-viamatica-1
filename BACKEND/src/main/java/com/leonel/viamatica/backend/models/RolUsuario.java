package com.leonel.viamatica.backend.models;

import jakarta.persistence.*;

@Entity
@Table(name = "rol_usuarios")
public class RolUsuario {
    @EmbeddedId
    private RolUsuarioId id;

    @MapsId("idRol")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "idRol", nullable = false)
    private Rol idRol;

    public RolUsuarioId getId() {
        return id;
    }

    public void setId(RolUsuarioId id) {
        this.id = id;
    }

    public Rol getIdRol() {
        return idRol;
    }

    public void setIdRol(Rol idRol) {
        this.idRol = idRol;
    }

}