package com.leonel.viamatica.backend.models;

import jakarta.persistence.*;

@Entity
@Table(name = "usuarios")
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idUsuario", nullable = false)
    private Integer id;

    @Column(name = "username", nullable = false, length = 50)
    private String username;

    @Column(name = "password", nullable = false, length = 50)
    private String password;

    @Column(name = "mail", nullable = false, length = 120)
    private String mail;

    @Column(name = "status", nullable = false)
    private Character status;

    @Column(name = "intentos_fallidos", length = 18)
    private String intentosFallidos;

    @Column(name = "max_intentos_fallidos", length = 18)
    private String maxIntentosFallidos;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idpersona")
    private Persona idpersona;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public String getIntentosFallidos() {
        return intentosFallidos;
    }

    public void setIntentosFallidos(String intentosFallidos) {
        this.intentosFallidos = intentosFallidos;
    }

    public String getMaxIntentosFallidos() {
        return maxIntentosFallidos;
    }

    public void setMaxIntentosFallidos(String maxIntentosFallidos) {
        this.maxIntentosFallidos = maxIntentosFallidos;
    }

    public Persona getIdpersona() {
        return idpersona;
    }

    public void setIdpersona(Persona idpersona) {
        this.idpersona = idpersona;
    }

}