package com.leonel.viamatica.backend.services;

import com.leonel.viamatica.backend.models.ParametrosSistema;
import com.leonel.viamatica.backend.repositories.ParametrosSistemaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ParametrosSistemaService {

    @Autowired
    private ParametrosSistemaRepository parametrosSistemaRepository;

    public List<ParametrosSistema> getAllParametros() {
        return parametrosSistemaRepository.findAll();
    }

    public Optional<ParametrosSistema> getParametroById(String id) {
        return parametrosSistemaRepository.findById(id);
    }

    public ParametrosSistema createParametro(ParametrosSistema parametro) {
        return parametrosSistemaRepository.save(parametro);
    }

    public ParametrosSistema updateParametro(String id, ParametrosSistema nuevoParametro) {
        if (parametrosSistemaRepository.existsById(id)) {
            nuevoParametro.setNombreParametro(id);
            return parametrosSistemaRepository.save(nuevoParametro);
        } else {
            throw new IllegalArgumentException("El parámetro con ID " + id + " no existe");
        }
    }

    public void deleteParametro(String id) {
        parametrosSistemaRepository.deleteById(id);
    }
}
