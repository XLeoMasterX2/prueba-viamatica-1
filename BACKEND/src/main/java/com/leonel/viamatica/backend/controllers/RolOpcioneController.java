package com.leonel.viamatica.backend.controllers;

import com.leonel.viamatica.backend.models.RolOpcione;
import com.leonel.viamatica.backend.services.RolOpcioneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/roles-opciones")
public class RolOpcioneController {

    @Autowired
    private RolOpcioneService rolOpcioneService;

    @GetMapping
    public List<RolOpcione> getAllRolOpciones() {
        return rolOpcioneService.getAllRolOpciones();
    }

    @GetMapping("/{id}")
    public ResponseEntity<RolOpcione> getRolOpcioneById(@PathVariable Integer id) {
        Optional<RolOpcione> rolOpcione = rolOpcioneService.getRolOpcioneById(id);
        return rolOpcione.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    public ResponseEntity<RolOpcione> createRolOpcione(@RequestBody RolOpcione rolOpcione) {
        RolOpcione createdRolOpcione = rolOpcioneService.createRolOpcione(rolOpcione);
        return new ResponseEntity<>(createdRolOpcione, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<RolOpcione> updateRolOpcione(@PathVariable Integer id, @RequestBody RolOpcione nuevaRolOpcione) {
        try {
            RolOpcione updatedRolOpcione = rolOpcioneService.updateRolOpcione(id, nuevaRolOpcione);
            return new ResponseEntity<>(updatedRolOpcione, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteRolOpcione(@PathVariable Integer id) {
        rolOpcioneService.deleteRolOpcione(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
