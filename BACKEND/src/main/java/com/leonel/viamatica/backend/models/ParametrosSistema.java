package com.leonel.viamatica.backend.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "Parametros_Sistema")
public class ParametrosSistema {
    @Id
    @Column(name = "nombre_parametro", nullable = false, length = 80)
    private String nombreParametro;

    @Column(name = "valor_Parametro", nullable = false, length = 120)
    private String valorParametro;

    public String getNombreParametro() {
        return nombreParametro;
    }

    public void setNombreParametro(String nombreParametro) {
        this.nombreParametro = nombreParametro;
    }

    public String getValorParametro() {
        return valorParametro;
    }

    public void setValorParametro(String valorParametro) {
        this.valorParametro = valorParametro;
    }

}