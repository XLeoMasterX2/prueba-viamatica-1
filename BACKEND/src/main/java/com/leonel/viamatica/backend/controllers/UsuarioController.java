package com.leonel.viamatica.backend.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.leonel.viamatica.backend.DTO.UsuarioDto;
import com.leonel.viamatica.backend.models.Usuario;
import com.leonel.viamatica.backend.services.UsuarioService;
import com.leonel.viamatica.backend.util.ConverterUtils;
import com.leonel.viamatica.backend.util.SchemaUtils;
import com.leonel.viamatica.backend.validations.JsonValidate;
import jakarta.persistence.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @GetMapping
    public List<Usuario> getAllUsuarios() {
        return usuarioService.getAllUsuarios();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Usuario> getUsuarioById(@PathVariable Integer id) {
        Optional<Usuario> usuario = usuarioService.getUsuarioById(id);
        return usuario.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    public ResponseEntity<?> createUsuario(@RequestBody UsuarioDto usuarioDTO) throws JsonProcessingException, ProcessingException {
        Usuario usuario = null;
        try {
            if (!JsonValidate.isValid(new ObjectMapper().valueToTree(usuarioDTO), SchemaUtils.UsuarioSchema)) {
                return ResponseEntity.badRequest().body("Error de validación: " + JsonValidate.getErrorMessage());
            }
            usuario = ConverterUtils.convertToEntity(usuarioDTO, Usuario.class);
            Usuario createdUsuario = usuarioService.createUsuario(usuario);
            return new ResponseEntity<>(createdUsuario, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>("Ha ocurrido un Error al Crear el Usuario", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Usuario> updateUsuario(@PathVariable Integer id, @RequestBody Usuario nuevoUsuario) {
        try {
            Usuario updatedUsuario = usuarioService.updateUsuario(id, nuevoUsuario);
            return new ResponseEntity<>(updatedUsuario, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUsuario(@PathVariable Integer id) {
        usuarioService.deleteUsuario(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
