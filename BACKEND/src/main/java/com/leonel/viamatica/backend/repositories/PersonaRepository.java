package com.leonel.viamatica.backend.repositories;

import com.leonel.viamatica.backend.models.Persona;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonaRepository extends JpaRepository<Persona, Integer> {
}