package com.leonel.viamatica.backend.services;

import com.leonel.viamatica.backend.models.Rol;
import com.leonel.viamatica.backend.repositories.RolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class RolService {

    @Autowired
    private RolRepository rolRepository;

    public List<Rol> getAllRoles() {
        return rolRepository.findAll();
    }

    public Optional<Rol> getRolById(Integer id) {
        return rolRepository.findById(id);
    }

    public Rol createRol(Rol rol) {
        return rolRepository.save(rol);
    }

    public Rol updateRol(Integer id, Rol nuevoRol) {
        if (rolRepository.existsById(id)) {
            nuevoRol.setId(id);
            return rolRepository.save(nuevoRol);
        } else {
            throw new IllegalArgumentException("El Rol con ID " + id + " no existe");
        }
    }

    public void deleteRol(Integer id) {
        rolRepository.deleteById(id);
    }
}
