package com.leonel.viamatica.backend.services;

import com.leonel.viamatica.backend.models.RolOpcione;
import com.leonel.viamatica.backend.repositories.RolOpcioneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class RolOpcioneService {

    @Autowired
    private RolOpcioneRepository rolOpcioneRepository;

    public List<RolOpcione> getAllRolOpciones() {
        return rolOpcioneRepository.findAll();
    }

    public Optional<RolOpcione> getRolOpcioneById(Integer id) {
        return rolOpcioneRepository.findById(id);
    }

    public RolOpcione createRolOpcione(RolOpcione rolOpcione) {
        return rolOpcioneRepository.save(rolOpcione);
    }

    public RolOpcione updateRolOpcione(Integer id, RolOpcione nuevaRolOpcione) {
        if (rolOpcioneRepository.existsById(id)) {
            nuevaRolOpcione.setId(id);
            return rolOpcioneRepository.save(nuevaRolOpcione);
        } else {
            throw new IllegalArgumentException("El Rol Opcion con ID " + id + " no existe");
        }
    }

    public void deleteRolOpcione(Integer id) {
        rolOpcioneRepository.deleteById(id);
    }
}
