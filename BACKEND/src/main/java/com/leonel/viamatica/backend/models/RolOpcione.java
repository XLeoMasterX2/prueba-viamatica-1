package com.leonel.viamatica.backend.models;

import jakarta.persistence.*;

@Entity
@Table(name = "rol_opciones")
public class RolOpcione {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idOpcion", nullable = false)
    private Integer id;

    @Column(name = "nombre_opcion", nullable = false, length = 50)
    private String nombreOpcion;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombreOpcion() {
        return nombreOpcion;
    }

    public void setNombreOpcion(String nombreOpcion) {
        this.nombreOpcion = nombreOpcion;
    }

}