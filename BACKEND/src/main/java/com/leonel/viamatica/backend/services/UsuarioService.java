package com.leonel.viamatica.backend.services;

import com.leonel.viamatica.backend.models.ParametrosSistema;
import com.leonel.viamatica.backend.models.Sesione;
import com.leonel.viamatica.backend.models.Usuario;
import com.leonel.viamatica.backend.repositories.SesioneRepository;
import com.leonel.viamatica.backend.repositories.UsuarioRepository;
import com.leonel.viamatica.backend.util.DefaultValues;
import com.leonel.viamatica.backend.util.Parameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private ParametrosSistemaService sistemaService;

    @Autowired
    private SesioneRepository sesioneRepository;

    public List<Usuario> getAllUsuarios() {
        return usuarioRepository.findAll();
    }

    public Optional<Usuario> getUsuarioById(Integer id) {
        return usuarioRepository.findById(id);
    }

    public Usuario createUsuario(Usuario usuario) {
        generarCorreo(usuario);
        usuario.setMaxIntentosFallidos(sistemaService.getParametroById(Parameters.MaxIntentosFallidos).map(ParametrosSistema::getValorParametro).orElse(DefaultValues.MaxIntentosFallidos));
        return usuarioRepository.save(usuario);
    }

    private void generarCorreo(Usuario usuario) {
        String nombres = usuario.getIdpersona().getNombres().toLowerCase();
        String apellidos = usuario.getIdpersona().getApellidos().toLowerCase();
        String identificacion = usuario.getIdpersona().getIdentificacion();

        String correoBase = nombres.charAt(0) + apellidos.charAt(0) + identificacion.substring(identificacion.length() - 4);
        String correo = correoBase + "@mail.com";

        int intentos = 1;
        while (usuarioRepository.existsByMail(correo)) {
            correo = correoBase + intentos + "@hotmail.com";
            intentos++;
        }

        usuario.setMail(correo);
    }

    public Usuario updateUsuario(Integer id, Usuario nuevoUsuario) {
        if (usuarioRepository.existsById(id)) {
            Usuario usuarioExistente = usuarioRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("El Usuario con ID " + id + " no existe"));

            // Verificar si el nuevo correo electrónico ya existe en otro usuario
            if (!nuevoUsuario.getMail().equals(usuarioExistente.getMail()) && usuarioRepository.existsByMail(nuevoUsuario.getMail())) {
                throw new IllegalArgumentException("El correo electrónico '" + nuevoUsuario.getMail() + "' ya está en uso por otro usuario.");
            }

            // Establecer el ID y guardar el usuario actualizado
            nuevoUsuario.setId(id);
            return usuarioRepository.save(nuevoUsuario);
        } else {
            throw new IllegalArgumentException("El Usuario con ID " + id + " no existe");
        }
    }

    public void deleteUsuario(Integer id) {
        Optional<Usuario> usuarioOptional = usuarioRepository.findById(id);

        if (usuarioOptional.isPresent()) {
            Usuario usuario = usuarioOptional.get();
            usuario.setStatus('0');
            usuarioRepository.save(usuario);
        } else {
            throw new IllegalArgumentException("El Usuario con ID " + id + " no existe");
        }
    }

    public Sesione validarCredenciales(String username, String password) {
        Usuario usuario = usuarioRepository.findByUsernameAndPassword(username, password);

        if (usuario != null)
        {
            Sesione sesione = new Sesione();
            sesione.setFechaIngreso(Instant.now());
            sesione.setIdUsuario(usuario);
            return sesioneRepository.save(sesione);
        }
        return null;
    }

    public boolean cerrarSesion(Integer sessionId) {
        Sesione sesion = sesioneRepository.findById(sessionId).orElse(null);

        if (Objects.nonNull(sesion) && Objects.nonNull(sesion.getFechaCierre())) {
            sesion.setFechaCierre(Instant.now());
            sesioneRepository.save(sesion);
            return true;
        }

        return false;
    }


}
