package com.leonel.viamatica.backend.models;

import jakarta.persistence.*;

@Entity
@Table(name = "rol_rol_opciones")
public class RolRolOpcione {
    @EmbeddedId
    private RolRolOpcioneId id;

    @MapsId("idRol")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "idRol", nullable = false)
    private Rol idRol;

    public RolRolOpcioneId getId() {
        return id;
    }

    public void setId(RolRolOpcioneId id) {
        this.id = id;
    }

    public Rol getIdRol() {
        return idRol;
    }

    public void setIdRol(Rol idRol) {
        this.idRol = idRol;
    }

}