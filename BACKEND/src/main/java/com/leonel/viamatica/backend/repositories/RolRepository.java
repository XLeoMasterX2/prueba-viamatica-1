package com.leonel.viamatica.backend.repositories;

import com.leonel.viamatica.backend.models.Rol;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RolRepository extends JpaRepository<Rol, Integer> {
}