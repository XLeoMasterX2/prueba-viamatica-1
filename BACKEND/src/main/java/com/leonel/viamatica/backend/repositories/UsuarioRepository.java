package com.leonel.viamatica.backend.repositories;

import com.leonel.viamatica.backend.models.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {
    boolean existsByUsername(String username);
    boolean existsByMail(String mail);
    Usuario findByUsernameAndPassword(String username, String password);


}