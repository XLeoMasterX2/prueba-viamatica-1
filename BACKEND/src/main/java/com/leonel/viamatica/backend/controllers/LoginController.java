package com.leonel.viamatica.backend.controllers;

import com.leonel.viamatica.backend.models.Sesione;
import com.leonel.viamatica.backend.repositories.SesioneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.leonel.viamatica.backend.services.UsuarioService;

import java.util.Objects;

@Controller
public class LoginController {

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private SesioneRepository sesioneRepository;

    @PostMapping("/login")
    @ResponseBody
    public ResponseEntity<?> procesarLogin(@RequestParam String username, @RequestParam String password) {
        Sesione sesionExistente = sesioneRepository.findFirstByIdUsuario_UsernameOrderByFechaIngresoDesc(username);

        if (sesionExistente != null) {
            if(Objects.nonNull(sesionExistente.getFechaCierre()))
                return new ResponseEntity<>("Ya tiene una sesión activa", HttpStatus.CONFLICT);
        }

        Sesione sesione = usuarioService.validarCredenciales(username, password);

        if (sesione != null) {
            return new ResponseEntity<>(sesione, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }


    @PostMapping("/logout")
    @ResponseBody
    public ResponseEntity<Void> procesarLogout(@RequestParam Integer sessionId) {
        boolean success = usuarioService.cerrarSesion(sessionId);

        if (success) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
