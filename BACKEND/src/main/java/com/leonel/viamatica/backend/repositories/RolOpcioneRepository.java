package com.leonel.viamatica.backend.repositories;

import com.leonel.viamatica.backend.models.RolOpcione;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RolOpcioneRepository extends JpaRepository<RolOpcione, Integer> {
}