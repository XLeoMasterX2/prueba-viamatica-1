package com.leonel.viamatica.backend.repositories;

import com.leonel.viamatica.backend.models.ParametrosSistema;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParametrosSistemaRepository extends JpaRepository<ParametrosSistema, String> {
}