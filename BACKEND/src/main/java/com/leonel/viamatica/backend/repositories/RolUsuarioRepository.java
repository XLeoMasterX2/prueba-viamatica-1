package com.leonel.viamatica.backend.repositories;

import com.leonel.viamatica.backend.models.RolUsuario;
import com.leonel.viamatica.backend.models.RolUsuarioId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RolUsuarioRepository extends JpaRepository<RolUsuario, RolUsuarioId> {
}